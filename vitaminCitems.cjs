// 2. Get all items containing only Vitamin C.
const items = require("./3-arrays-vitamins.cjs");
console.log(items);
function vitamin_C_items(items) {
    // Solved Using the Reduce function

    let ans_arr = [];
    let itemsOfVitaminC = items.reduce((emptyArr, itemsObj) => {
        console.log(itemsObj, "items Obj kya hia");
        let splitContains = itemsObj.contains.split(",");
        splitContains.map((item, i) => {
            if(item.includes("Vitamin C")){
                ans_arr.push(itemsObj);
            }
        })
    },)
    return ans_arr;


    // Solved using the map function
    // let vitCArr = [];
    // items.map((item) => {
    //     if (item["contains"].indexOf("C") > -1) {
    //         vitCArr.push(item.name);
    //     }
    // });
    // return vitCArr;
}
console.log(vitamin_C_items(items));