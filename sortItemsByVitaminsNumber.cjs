// 5. Sort items based on number of Vitamins they contain.
const items = require("./3-arrays-vitamins.cjs");
function sortItemsByVitaminsNumber(items) {
    let vitaminNumberCount = items.reduce((emptyAnsArr, itemsObj) => {
        if (itemsObj.contains.indexOf(',') > -1) {
            let vitaminArray = itemsObj.contains.split(", ");
            emptyAnsArr.push({ Fruit: itemsObj.name, Number_of_Vitamins: vitaminArray.length });
        }
        else {
            emptyAnsArr.push({ Fruit: itemsObj.name, Number_of_Vitamins: 1 });
        }
        return emptyAnsArr;
    }, []).sort((firstFruit, secondFruit) => {
        if (firstFruit.Number_of_Vitamins > secondFruit.Number_of_Vitamins) {
            return -1;
        }
        else {
            return 1;
        }
    });
    return vitaminNumberCount;


    // const sortItemsByVitaminNum = items.sort((item1, item2) => {
    //     if (item1.contains.split("Vitamin").length > item2.contains.split("Vitamin").length) {
    //         return 1;
    //     } else {
    //         return -1;
    //     }
    // });
    // return items;
}
console.log(sortItemsByVitaminsNumber(items));