// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }
// and so on for all items and all Vitamins.
const items = require("./3-arrays-vitamins.cjs");
console.log(items);
function groupItems_(items) {
    let ans_obj = items.reduce((emptyArr, itemsObj) => {
        if (itemsObj.contains.indexOf(',') > -1) {
            let vitaminArray = itemsObj.contains.split(", ");
            vitaminArray.map((singleVitamin) => {
                if (emptyArr.hasOwnProperty(singleVitamin)) {
                    emptyArr[singleVitamin].push(itemsObj.name);
                }
                else {
                    emptyArr[singleVitamin] = [];
                    emptyArr[singleVitamin].push(itemsObj.name);
                }
            });
        }
        else {
            if (emptyArr.hasOwnProperty(itemsObj.contains)) {
                emptyArr[itemsObj.contains].push(itemsObj.name);
            }
            else {
                emptyArr[itemsObj.contains] = [itemsObj.name];
            }
        }
        return emptyArr;
    }, {})
    return ans_obj;


    // Solved using the Reduce function.
    // let vitaminBased_items = items.reduce((allVitamin, eachVitamin) => {
    //     eachVitamin.contains.split(", ")
    //         .map((eachElement) => {
    //             if (allVitamin[eachElement] === undefined) {
    //                 allVitamin[eachElement] = [];
    //                 allVitamin[eachElement].push(eachVitamin.name);
    //                 allVitamin[eachElement].push("Yese bhi push kar sakte hai...");
    //             } else {
    //                 allVitamin[eachElement].push(eachVitamin.name);
    //             }
    //         });
    //     return allVitamin;
    // }, {});
    // return vitaminBased_items;
}
console.log(groupItems_(items));