// 3. Get all items containing Vitamin A.

const items = require("./3-arrays-vitamins.cjs");
console.log(items);
function vitamin_A_items(items) {
    let vitamin_A_items = [];
    items.filter((item) => {
        if (item.contains.indexOf("A") > -1) {
            vitamin_A_items.push(item.name);
        }
    });
    return vitamin_A_items;
}
console.log(vitamin_A_items(items));