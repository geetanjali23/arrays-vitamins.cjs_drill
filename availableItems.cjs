// 1. Get all items that are available 
const items = require("./3-arrays-vitamins.cjs");
function availableItems(items) {
    //Solved using reduce function
    // let itemsAvailable = items.reduce((emptyArr, arrObj) => {
    //     if (arrObj.available) {
    //         emptyArr.ans_arr.push(arrObj.name);
    //     }
    //     return emptyArr;
    // }, { ans_arr: [] })
    // return itemsAvailable;

    //Solved using filter function
    // let allAvailabeItems = items.filter((availableItem) => {

    //     return availableItem.available === true;
    // })
    // return allAvailabeItems;

    // Solved using map function
    let itemsArray = [];
    console.log(items);
    items.map((items, i) => {
        if (items.available) {
            itemsArray.push(items.name);
        }
    });
    return itemsArray;
}
console.log(availableItems(items));

